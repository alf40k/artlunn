# README #
### Summary ###
This is the repo tracking the development of my personal website. Feel free to take a look! I'm just learning so if you have any recommendations on design or code I'm always willing to hear them!

You can check the site out at ArtLunn.com

v1.0

### Development ###

This is the first working version of the site so things are still very rough around the edges.
Currently website is working on Chrome in full screen. Steps need to be taken to get the program working across platforms and screen sizes. The following tasks still need to be taken care of and are high priority: 

1. Stabilize behavior across platforms 
2. Stabilize behavior across window dimensions 
3. Reformat, rearrange, cleanup, and generally improve code layout and readability.  

The following tasks are of medium priority: 

4. Research better animation for the project tiles "flip" 
5. Implement parallax scrolling on header image and background pattern  
 
The following tasks are long term goals and are of low priority: 

6. Implement blog functionality using Django 

### Attribution ###
In order to implement the "flip" function the following post was referenced:

https://davidwalsh.name/css-flip

In order to achieve menu re-sizing the following post was referenced:

http://www.webdesignerdepot.com/2013/03/how-to-create-a-resizing-menu-bar/

Both of these effects are quick and dirty solutions. More time needs to be spent developing solutions that will better serve my purposes.

### Contact ###

Please contact alf40k@gmail.com or ArthurLLunn@gmail.com with any questions or comments!